This is a small collection of useful scripts that I have come across/modified/created for Mikrotik routers

Attributions have been included where I remember them, apologies if I have missed anything

Everything in this repository is provided without any warranty and with no restrictions on use, modification or distribution