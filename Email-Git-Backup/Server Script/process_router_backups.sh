#!/bin/bash

# This is a small app which takes the router configs from the router configs mailbox (all Mikrotik routers email their config to here daily)
# commits to the local Git repo, and then pushes a copy up to bitbucket

git_repo_path="your-git-repo-path"
mail_server="your-pop3-mail-server"
mail_username="your-mailbox-username"
mail_password="your-mailbox-password"

# *** Do not change anything below this line ***

export GIT_WORK_TREE=${git_repo_path}
export GIT_DIR=${git_repo_path}/.git 

# Run a git pull to bring in any manual modifications to the repo
git pull

# Pull out the attachments
mkdir -p /tmp/router_configs
cd /tmp/router_configs
rm *
$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/leechpop.pl --server=${mail_server} --username=${mail_username} --password=${mail_password}
mv * ${git_repo_path}

git add -f *
git commit --author="Router Configs <router-configs@mikrotik.com>" --message="Automatic daily router configuration backups"
git push
