This is a two part script - part one runs on a Mikrotik router (set up with a scheduler to run daily) which will email a copy of the current running config (in both text and binary formats) to a special-purpose mailbox and part two polls that mailbox, extracts the attachments and saves them to a Git repository

The server component runs using bash and Perl. Perl requires Net::POP3 and MIME::Parser.

Set up your git repository before use including the remote to push the backups to (if you don't want to use a remote repository, comment out the "git pull" and "git push" lines in process_router_backups.sh